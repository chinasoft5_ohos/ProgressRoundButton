/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xiaochen.progressroundbutton.utils;

import ohos.agp.components.AttrSet;

import ohos.app.Context;

/**
 * 自定义属性工具类
 * <p>
 * 注意： 宽高都为 match_content 且无实际内容时构造方法不会调用
 * 使用方法：
 * xxx extends Component
 * 获取自定义属性：
 * String count = AttrUtils.getStringFromAttr(attrSet,"cus_count","0");
 * <p>
 * 属性定义：
 * 布局头中加入  xmlns:hap="http://schemas.huawei.com/apk/res/ohos" 使用hap区分自定义属性与系统属性
 * 即可使用 hap:cus_count="2"  不加直接使用ohos:cus_count="2"
 * <p>
 * 与xxx区别：xxx在xml预先定义好属性，可通过app: 点选，ohos暂未发现对应方法
 */
public class AttrUtils {
    /**
     * getString
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return String
     */
    public static String getString(AttrSet attrSet, String name, String defaultValue) {
        String value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getStringValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * getInteger
     *
     * @param context
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return Integer
     */
    public static Integer getInteger(Context context, AttrSet attrSet, String name, Integer defaultValue) {
        Integer value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getIntegerValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * getFloat
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return float
     */
    public static float getFloat(AttrSet attrSet, String name, float defaultValue) {
        float value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getFloatValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * getBoolean
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return boolean
     */
    public static boolean getBoolean(AttrSet attrSet, String name, boolean defaultValue) {
        boolean value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getBoolValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * getLong
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return Long
     */
    public static Long getLong(AttrSet attrSet, String name, Long defaultValue) {
        Long value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getLongValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    /**
     * getColor
     *
     * @param attrSet
     * @param name
     * @param defaultValue
     * @return 色值
     */
    public static int getColor(AttrSet attrSet, String name, int defaultValue) {
        int value = defaultValue;
        try {
            if (attrSet.getAttr(name) != null && attrSet.getAttr(name).isPresent()) {
                value = attrSet.getAttr(name).get().getColorValue().getValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }
}
