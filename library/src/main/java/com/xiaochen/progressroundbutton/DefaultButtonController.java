package com.xiaochen.progressroundbutton;

import com.xiaochen.progressroundbutton.utils.ColorUtil;

/**
 * Created by tanfujun on 10/26/16.
 */
public class DefaultButtonController implements ButtonController {
    private boolean enablePress;
    private boolean enableGradient;

    /**
     * 获得按下的颜色（明度降低10%）
     *
     * @param color 颜色色值
     * @return int
     */
    public int getPressedColor(int color) {
        return ColorUtil.colorAtLightness(color, 0.8f);
    }

    /**
     * 由右边的颜色算出左边的颜色（左边的颜色比右边的颜色降饱和度30%，亮度增加30%）
     * +
     *
     * @param color 颜色色值
     * @return int
     */
    public int getLighterColor(int color) {
        return ColorUtil.colorAtLightness(color, 0.95f);
    }

    /**
     * 由左边的颜色生成右边的颜色
     *
     * @param color 颜色色值
     * @return int
     */
    public int getDarkerColor(int color) {
        return ColorUtil.colorAtLightness(color, 0.3f);
    }

    @Override
    public boolean enablePress() {
        return enablePress;
    }

    @Override
    public boolean enableGradient() {
        return enableGradient;
    }

    public DefaultButtonController setEnablePress(boolean enablePress) {
        this.enablePress = enablePress;
        return this;
    }

    public DefaultButtonController setEnableGradient(boolean enableGradient) {
        this.enableGradient = enableGradient;
        return this;
    }
}
