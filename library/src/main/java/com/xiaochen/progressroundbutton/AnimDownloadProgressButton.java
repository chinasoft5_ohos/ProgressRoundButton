package com.xiaochen.progressroundbutton;

import com.xiaochen.progressroundbutton.utils.AttrUtils;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.render.Canvas;
import ohos.agp.render.LinearShader;
import ohos.agp.render.Paint;
import ohos.agp.render.Shader;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.RectFloat;

import ohos.app.Context;

import ohos.multimodalinput.event.TouchEvent;

/**
 * Created by tanfujun on 15/9/4.
 */
public class AnimDownloadProgressButton extends Text implements Component.DrawTask, Component.TouchEventListener {
    /**
     * 普通状态
     */
    public static final int NORMAL = 0;
    /**
     * 下载中
     */
    public static final int DOWNLOADING = 1;
    /**
     * 有点运动状态
     */
    public static final int INSTALLING = 2;
    /**
     * 背景画笔
     */
    private Paint mBackgroundPaint;
    /**
     * 按钮文字画笔
     */
    private volatile Paint mTextPaint;
    /**
     * 第一个点画笔
     */
    private Paint mDot1Paint;
    /**
     * 第二个点画笔
     */
    private Paint mDot2Paint;
    /**
     * 背景颜色
     */
    private Color[] mBackgroundColor;
    private Color[] mOriginBackgroundColor;
    /**
     * 下载中后半部分后面背景颜色
     */
    private int mBackgroundSecondColor;
    /**
     * 文字颜色
     */
    private Color mTextColor;
    /**
     * 覆盖后颜色
     */
    private int mTextCoverColor;
    /**
     * 文字大小
     */
    private int mAboveTextSize = 50;

    private float mProgress = -1;
    private float mToProgress;
    private int mMaxProgress;
    private int mMinProgress;
    private float mProgressPercent;
    private float mButtonRadius;

    /**
     * 两个点向右移动距离
     */
    private float mDot1transX;
    private float mDot2transX;

    private RectFloat mBackgroundBounds;
    private LinearShader mFillBgGradient;
    private LinearShader mProgressBgGradient;

    /**
     * 点运动动画
     */
    private AnimatorGroup mDotAnimationSet;
    /**
     * 下载平滑动画
     */
    private AnimatorValue mProgressAnimation;
    /**
     * 记录当前文字
     */
    private CharSequence mCurrentText;

    private ButtonController mDefaultController;

    private ButtonController mCustomerController;

    private int mState;
    private Canvas mCanvas;
    private boolean isPressed = false;

    public AnimDownloadProgressButton(Context context) {
        this(context, null);
    }

    public AnimDownloadProgressButton(Context context, AttrSet attrs) {
        super(context, attrs);

        mContext = context;
        initController();
        initAttrs(context, attrs);
        init();
        // 添加绘制任务
        setupAnimations();
        // 设置TouchEvent响应事件
        setTouchEventListener(this);
        addDrawTask(this);
    }

    private void initController() {
        mDefaultController = new DefaultButtonController();
    }

    private void initAttrs(Context context, AttrSet attrs) {
        int bgColor = AttrUtils.getColor(attrs, "progressbtn_background_color", Color.getIntColor("#6699ff"));
        initGradientColor(bgColor, bgColor);
        mBackgroundSecondColor = AttrUtils.getColor(attrs, "progressbtn_background_second_color", 0xFFCCCCCC);
        mButtonRadius = AttrUtils.getFloat(attrs, "progressbtn_radius", getHeight() / 2);
        mAboveTextSize = AttrUtils.getInteger(context, attrs, "progressbtn_text_size", 50);
        mTextColor = new Color(AttrUtils.getColor(attrs, "progressbtn_text_color", bgColor));
        mTextCoverColor = AttrUtils.getColor(attrs, "progressbtn_text_covercolor", Color.WHITE.getValue());
        boolean enableGradient = AttrUtils.getBoolean(attrs, "progressbtn_enable_gradient", false);
        boolean enablePress = AttrUtils.getBoolean(attrs, "progressbtn_enable_press", false);
        ((DefaultButtonController) mDefaultController).setEnableGradient(enableGradient).setEnablePress(enablePress);
        if (enableGradient) {
            initGradientColor(mDefaultController.getLighterColor(mBackgroundColor[0].getValue()), mBackgroundColor[0].getValue());
        }
    }

    private void init() {
        mMaxProgress = 100;
        mMinProgress = 0;
        mProgress = 0;

        // 设置背景画笔
        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mBackgroundPaint.setStyle(Paint.Style.FILL_STYLE);

        // 设置文字画笔
        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setTextSize(mAboveTextSize);

        // 设置第一个点画笔
        mDot1Paint = new Paint();
        mDot1Paint.setAntiAlias(true);
        mDot1Paint.setTextSize(mAboveTextSize);

        // 设置第二个点画笔
        mDot2Paint = new Paint();
        mDot2Paint.setAntiAlias(true);
        mDot2Paint.setTextSize(mAboveTextSize);

        // 初始化状态设为NORMAL
        mState = NORMAL;
        invalidate();
    }

    /**
     * 初始化渐变色
     *
     * @param leftColor  左边渐变色
     * @param rightColor 右边渐变色
     * @return Color[]
     */
    private Color[] initGradientColor(int leftColor, int rightColor) {
        mBackgroundColor = new Color[2];
        mBackgroundColor[0] = new Color(leftColor);
        mBackgroundColor[1] = new Color(rightColor);
        return mBackgroundColor;
    }

    private void setupAnimations() {
        // 两个点向右移动动画
        AnimatorValue dotMoveAnimation = dotMoveAnimation();
        // 两个点隐藏显示动画
        AnimatorValue dotAlphaAnim = dotAlphaAnim();

        dotAlphaAnim.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                mDot1Paint.setAlpha(0);
                mDot2Paint.setAlpha(0);
            }

            @Override
            public void onStop(Animator animator) {

            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        // 两个点的动画集合
        mDotAnimationSet = new AnimatorGroup();
        mDotAnimationSet.runParallel(dotAlphaAnim, dotMoveAnimation);
        // ProgressBar的动画
        mProgressAnimation = new AnimatorValue();
        mProgressAnimation.setDuration(500);
        mProgressAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float timepercent = v;
                mProgress = ((mToProgress - mProgress) * timepercent + mProgress);
                invalidate();
            }
        });
    }

    private AnimatorValue dotAlphaAnim() {
        AnimatorValue dotAlphaAnim = new AnimatorValue();
        dotAlphaAnim.setDuration(1234);
        dotAlphaAnim.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                int time = (int) (1234 * v);
                int dot1Alpha = calculateDot1AlphaByTime(time);
                int dot2Alpha = calculateDot2AlphaByTime(time);
                mDot1Paint.setColor(Color.WHITE);
                mDot2Paint.setColor(Color.WHITE);
                mDot1Paint.setAlpha(dot1Alpha);
                mDot2Paint.setAlpha(dot2Alpha);
            }
        });
        dotAlphaAnim.setLoopedCount(Animator.INFINITE);
        return dotAlphaAnim;
    }

    private AnimatorValue dotMoveAnimation() {
        AnimatorValue dotMoveAnimation = new AnimatorValue();
        dotMoveAnimation.setCurveType(Animator.CurveType.CUBIC_BEZIER_STANDARD);
        dotMoveAnimation.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
            @Override
            public void onUpdate(AnimatorValue animatorValue, float v) {
                float transX = (20 * v);
                mDot1transX = transX;
                mDot2transX = transX;
                invalidate();
            }
        });
        dotMoveAnimation.setDuration(1243);
        dotMoveAnimation.setLoopedCount(Animator.INFINITE);
        return dotMoveAnimation;
    }

    /**
     * 第一个点透明度计算函数
     *
     * @param time 时间
     * @return int
     */
    private int calculateDot2AlphaByTime(int time) {
        int alpha;
        if (0 <= time && time <= 83) {
            double dotAlpha = 255.0 / 83.0 * time;
            alpha = (int) dotAlpha;
        } else if (83 < time && time <= 1000) {
            alpha = 255;
        } else if (1000 < time && time <= 1083) {
            double dotAlpha = -255.0 / 83.0 * (time - 1083);
            alpha = (int) dotAlpha;
        } else if (1083 < time && time <= 1243) {
            alpha = 0;
        } else {
            alpha = 255;
        }
        return alpha;
    }

    /**
     * 第二个点透明度计算函数
     *
     * @param time 时间
     * @return int
     */
    private int calculateDot1AlphaByTime(int time) {
        int alpha;
        if (0 <= time && time <= 160) {
            alpha = 0;
        } else if (160 < time && time <= 243) {
            double dotAlpha = 255.0 / 83.0 * (time - 160);
            alpha = (int) dotAlpha;
        } else if (243 < time && time <= 1160) {
            alpha = 255;
        } else if (1160 < time && time <= 1243) {
            double dotAlpha = -255.0 / 83.0 * (time - 1243);
            alpha = (int) dotAlpha;
        } else {
            alpha = 255;
        }
        return alpha;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        this.mCanvas = canvas;
        if (isPressed) {
            isDraw();
        } else {
            drawing(canvas);
        }
    }

    private void drawableStateChanged() {
        ButtonController buttonController = switchController();
        if (buttonController.enablePress()) {
            if (mOriginBackgroundColor == null) {
                mOriginBackgroundColor = new Color[2];
                mOriginBackgroundColor[0] = mBackgroundColor[0];
                mOriginBackgroundColor[1] = mBackgroundColor[1];
            }
            if (this.isPressed()) {
                int pressColorleft = mDefaultController.getPressedColor(mBackgroundColor[0].getValue());
                int pressColorright = mDefaultController.getPressedColor(mBackgroundColor[1].getValue());
                if (buttonController.enableGradient()) {
                    initGradientColor(pressColorleft, pressColorright);
                } else {
                    initGradientColor(pressColorleft, pressColorleft);
                }
            } else {
                if (buttonController.enableGradient()) {
                    initGradientColor(mOriginBackgroundColor[0].getValue(), mOriginBackgroundColor[1].getValue());
                } else {
                    initGradientColor(mOriginBackgroundColor[0].getValue(), mOriginBackgroundColor[0].getValue());
                }
            }
            invalidate();
        }
    }

    private void drawableStateChanged(boolean isPressed) {
        ButtonController buttonController = switchController();
        if (buttonController.enablePress()) {
            if (mOriginBackgroundColor == null) {
                mOriginBackgroundColor = new Color[2];
                mOriginBackgroundColor[0] = mBackgroundColor[0];
                mOriginBackgroundColor[1] = mBackgroundColor[1];
            }
            if (isPressed) {
                int pressColorleft = mDefaultController.getPressedColor(mBackgroundColor[0].getValue());
                int pressColorright = mDefaultController.getPressedColor(mBackgroundColor[1].getValue());
                if (buttonController.enableGradient()) {
                    initGradientColor(pressColorleft, pressColorright);
                } else {
                    initGradientColor(pressColorleft, pressColorleft);
                }
            } else {
                if (buttonController.enableGradient()) {
                    initGradientColor(mOriginBackgroundColor[0].getValue(), mOriginBackgroundColor[1].getValue());
                } else {
                    initGradientColor(mOriginBackgroundColor[0].getValue(), mOriginBackgroundColor[0].getValue());
                }
            }
            invalidate();
        }
    }

    private void drawing(Canvas canvas) {
        drawableStateChanged();
        drawBackground(canvas);
        drawTextAbove(canvas);
    }

    private void drawBackground(Canvas canvas) {
        mBackgroundBounds = new RectFloat();
        if (mButtonRadius == 0) {
            mButtonRadius = getHeight() / 2;
        }
        mBackgroundBounds.left = 2;
        mBackgroundBounds.top = 2;
        mBackgroundBounds.right = getWidth() - 2;
        mBackgroundBounds.bottom = getHeight() - 2;

        ButtonController buttonController = switchController();
        switch (mState) {
            case NORMAL:
                normal(canvas, buttonController);
                break;
            case DOWNLOADING:
                downloading(canvas, buttonController);
                break;
            case INSTALLING:
                installing(canvas, buttonController);
                break;
        }
    }

    private void normal(Canvas canvas, ButtonController buttonController) {
        if (buttonController.enableGradient()) {
            Point point1 = new Point(0, getHeight() / 2);
            Point point2 = new Point(getWidth(), getHeight() / 2);
            Point[] points = {point1, point2};
            Color[] colors = mBackgroundColor;
            mFillBgGradient = new LinearShader(points, null, colors, Shader.TileMode.CLAMP_TILEMODE);
            mBackgroundPaint.setShader(mFillBgGradient, Paint.ShaderType.LINEAR_SHADER);
        } else {
            if (mBackgroundPaint.getShader() != null) {
                mBackgroundPaint.setShader(null, Paint.ShaderType.LINEAR_SHADER);
            }
            mBackgroundPaint.setColor(mBackgroundColor[0]);
        }
        canvas.drawRoundRect(mBackgroundBounds, mButtonRadius, mButtonRadius, mBackgroundPaint);
    }

    private void downloading(Canvas canvas, ButtonController buttonController) {
        if (buttonController.enableGradient()) {
            mProgressPercent = mProgress / (mMaxProgress + 0f);
            Point point1 = new Point(0, 0);
            Point point2 = new Point(getWidth(), 0);
            Point[] points = {point1, point2};
            Color[] colors = {mBackgroundColor[0], mBackgroundColor[1], new Color(mBackgroundSecondColor)};
            float[] newStops = new float[]{0, mProgressPercent, mProgressPercent + 0.001f};
            mProgressBgGradient = new LinearShader(points, newStops, colors, Shader.TileMode.CLAMP_TILEMODE);
            mBackgroundPaint.setShader(mProgressBgGradient, Paint.ShaderType.LINEAR_SHADER);
        } else {
            mProgressPercent = mProgress / (mMaxProgress + 0f);
            Point point1 = new Point(0, 0);
            Point point2 = new Point(getWidth(), 0);
            Point[] points = {point1, point2};
            Color[] colors = {mBackgroundColor[0], new Color(mBackgroundSecondColor)};
            float[] newStops = {mProgressPercent, mProgressPercent + 0.001f};
            mProgressBgGradient = new LinearShader(points, newStops, colors, Shader.TileMode.CLAMP_TILEMODE);
            mBackgroundPaint.setColor(mBackgroundColor[0]);
            mBackgroundPaint.setShader(mProgressBgGradient, Paint.ShaderType.LINEAR_SHADER);
        }
        canvas.drawRoundRect(mBackgroundBounds, mButtonRadius, mButtonRadius, mBackgroundPaint);
    }

    private void installing(Canvas canvas, ButtonController buttonController) {
        if (buttonController.enableGradient()) {
            Point point1 = new Point(0, getHeight() / 2);
            Point point2 = new Point(getWidth(), getWidth() / 2);
            Point[] points = {point1, point2};
            Color[] colors = mBackgroundColor;
            mFillBgGradient = new LinearShader(points, null, colors, Shader.TileMode.CLAMP_TILEMODE);
            mBackgroundPaint.setShader(mFillBgGradient, Paint.ShaderType.LINEAR_SHADER);
        } else {
            mBackgroundPaint.setShader(null, Paint.ShaderType.LINEAR_SHADER);
            mBackgroundPaint.setColor(mBackgroundColor[0]);
        }
        canvas.drawRoundRect(mBackgroundBounds, mButtonRadius, mButtonRadius, mBackgroundPaint);
    }

    private void drawTextAbove(Canvas canvas) {
        final float y = getHeight() / 2 - (mTextPaint.descent() / 2 + mTextPaint.ascent() / 2);
        if (mCurrentText == null) {
            mCurrentText = "";
        }
        final float textWidth = mTextPaint.measureText(mCurrentText.toString());
        // color
        switch (mState) {
            case NORMAL:
                mTextPaint.setShader(null, Paint.ShaderType.LINEAR_SHADER);
                mTextPaint.setColor(new Color(mTextCoverColor));
                canvas.drawText(mTextPaint, mCurrentText.toString(), (getWidth() - textWidth) / 2, y);
                break;
            case DOWNLOADING:
                //进度条压过距离
                float coverlength = getWidth() * mProgressPercent;
                //开始渐变指示器
                float indicator1 = getWidth() / 2 - textWidth / 2;
                //结束渐变指示器
                float indicator2 = getWidth() / 2 + textWidth / 2;
                //文字变色部分的距离
                float coverTextLength = textWidth / 2 - getWidth() / 2 + coverlength;
                float textProgress = coverTextLength / textWidth;
                if (coverlength <= indicator1) {
                    mTextPaint.setShader(null, Paint.ShaderType.LINEAR_SHADER);
                    mTextPaint.setColor(mTextColor);
                } else if (indicator1 < coverlength && coverlength <= indicator2) {

                    Point point1 = new Point((getWidth() - textWidth) / 2, 0);
                    Point point2 = new Point((getWidth() + textWidth) / 2, 0);
                    Point[] points = {point1, point2};
                    Color[] colors = {new Color(mTextCoverColor), mTextColor};
                    float[] newStops = {textProgress, textProgress + 0.001f};
                    Shader mProgressTextGradient = new LinearShader(points, newStops, colors, Shader.TileMode.CLAMP_TILEMODE);
                    mTextPaint.setColor(mTextColor);
                    mTextPaint.setShader(mProgressTextGradient, Paint.ShaderType.LINEAR_SHADER);
                } else {
                    mTextPaint.setShader(null, Paint.ShaderType.LINEAR_SHADER);
                    mTextPaint.setColor(new Color(mTextCoverColor));
                }
                canvas.drawText(mTextPaint, mCurrentText.toString(), (getWidth() - textWidth) / 2, y);
                break;
            case INSTALLING:
                mTextPaint.setColor(new Color(mTextCoverColor));
                canvas.drawText(mTextPaint, mCurrentText.toString(), (getWidth() - textWidth) / 2, y);
                canvas.drawCircle((getWidth() + textWidth) / 2 + 4 + mDot1transX, y, 4, mDot1Paint);
                canvas.drawCircle((getWidth() + textWidth) / 2 + 24 + mDot2transX, y, 4, mDot2Paint);
                break;
        }
    }

    private ButtonController switchController() {
        if (mCustomerController != null) {
            return mCustomerController;
        } else {
            return mDefaultController;
        }
    }

    /**
     * 获取按钮当前进行状态
     *
     * @return int
     */
    public int getState() {
        return mState;
    }

    /**
     * 设置按钮当前进行状态
     *
     * @param state 状态值
     */
    public void setState(int state) {
        if (mState != state) {
            // 状态确实有改变
            this.mState = state;
            invalidate();
            if (state == AnimDownloadProgressButton.INSTALLING) {
                // 开启两个点动画
                mDotAnimationSet.start();
            } else if (state == NORMAL) {
                mDotAnimationSet.cancel();
            } else if (state == DOWNLOADING) {
                mDotAnimationSet.cancel();
            } else {
            }
        }
    }

    /**
     * 是否绘制
     */
    public void isDraw() {
        isPressed = true;
        drawableStateChanged(true);
        drawBackground(mCanvas);
        drawTextAbove(mCanvas);
    }

    /**
     * 设置绘制
     */
    public void setDraw() {
        isPressed = false;
    }

    /**
     * 设置按钮文字
     *
     * @param charSequence 文本
     */
    public void setCurrentText(CharSequence charSequence) {
        mCurrentText = charSequence;
        invalidate();
    }

    /**
     * 设置带下载进度的文字
     *
     * @param text     文本
     * @param progress 进度值
     */
    public void setProgressText(String text, float progress) {
        if (progress >= mMinProgress && progress < mMaxProgress) {
            mCurrentText = text + (int) progress + " " + "%";
            mToProgress = progress;
            if (mProgressAnimation.isRunning()) {
                mProgressAnimation.start();
            } else {
                mProgressAnimation.start();
            }
        } else if (progress < mMinProgress) {
            mProgress = 0;
        } else if (progress >= mMaxProgress) {
            mProgress = 100;
            mCurrentText = text + " " + (int) mProgress + "%";
            invalidate();
        } else {
        }
    }

    /**
     * 获取按钮当前进度值
     *
     * @return 进度值
     */
    public float getProgress() {
        return mProgress;
    }

    /**
     * 设置按钮当前进度值
     *
     * @param progress 进度值
     */
    public void setProgress(float progress) {
        this.mProgress = progress;
    }

    /**
     * Sometimes you should use the method to avoid memory leak
     */
    public void removeAllAnim() {
        mDotAnimationSet.cancel();
        mProgressAnimation.cancel();
    }

    /**
     * 设置按钮进度背景色
     *
     * @param color 颜色色值
     */
    public void setProgressBtnBackgroundColor(int color) {
        initGradientColor(color, color);
        invalidate();
    }

    /**
     * 设置按钮进度未进行的背景色
     *
     * @param color 颜色色值
     */
    public void setProgressBtnBackgroundSecondColor(int color) {
        mBackgroundSecondColor = color;
    }

    /**
     * 获取按钮圆角
     *
     * @return 圆角值
     */
    public float getButtonRadius() {
        return mButtonRadius;
    }

    /**
     * 设置按钮圆角
     *
     * @param buttonRadius 圆角值
     */
    public void setButtonRadius(float buttonRadius) {
        mButtonRadius = buttonRadius;
    }

    /**
     * 获取按钮文字颜色
     *
     * @return 颜色色值
     */
    public Color getTextColor() {
        return mTextColor;
    }

    /**
     * 设置文字颜色
     *
     * @param color 颜色色值
     */
    @Override
    public void setTextColor(Color color) {
        super.setTextColor(color);
        mTextColor = color;
    }

    /**
     * 获取文字转变色
     *
     * @return 颜色色值
     */
    public int getTextCoverColor() {
        return mTextCoverColor;
    }

    /**
     * 色值文字转变色
     *
     * @param textCoverColor 颜色色值
     */
    public void setTextCoverColor(int textCoverColor) {
        mTextCoverColor = textCoverColor;
    }

    /**
     * 获取最小进度值
     *
     * @return 进度值
     */
    public int getMinProgress() {
        return mMinProgress;
    }

    /**
     * 设置最小进度值
     *
     * @param minProgress 进度值
     */
    public void setMinProgress(int minProgress) {
        mMinProgress = minProgress;
    }

    /**
     * 获取最大进度值
     *
     * @return 进度值
     */
    public int getMaxProgress() {
        return mMaxProgress;
    }

    /**
     * 设置最大进度值
     *
     * @param maxProgress 进度值
     */
    public void setMaxProgress(int maxProgress) {
        mMaxProgress = maxProgress;
    }

    /**
     * 是否设置默认按下
     *
     * @param enable 是否
     */
    public void enabelDefaultPress(boolean enable) {
        if (mDefaultController != null) {
            ((DefaultButtonController) mDefaultController).setEnablePress(enable);
        }
    }

    /**
     * 是否设置默认渐变色
     *
     * @param enable 是否
     */
    public void enabelDefaultGradient(boolean enable) {
        if (mDefaultController != null) {
            ((DefaultButtonController) mDefaultController).setEnableGradient(enable);
            initGradientColor(mDefaultController.getLighterColor(mBackgroundColor[0].getValue()), mBackgroundColor[0].getValue());
        }
    }

    /**
     * 设置文字大小
     *
     * @param size 文字大小
     */
    @Override
    public void setTextSize(int size) {
        mAboveTextSize = size;
        mTextPaint.setTextSize(size);
    }

    /**
     * 获取文字大小
     *
     * @return 文字大小
     */
    @Override
    public int getTextSize() {
        return mAboveTextSize;
    }

    /**
     * 设置自定义控制器
     *
     * @param customerController 自定义控制器
     * @return AnimDownloadProgressButton
     */
    public AnimDownloadProgressButton setCustomerController(ButtonController customerController) {
        mCustomerController = customerController;
        return this;
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        if (!isEnabled()) {
            return false;
        }
        if (!isClickable()) {
            return false;
        }
        int action = touchEvent.getAction();
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                isPressed = false;
                drawing(mCanvas);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                break;
            case TouchEvent.POINT_MOVE:
                break;
            case TouchEvent.CANCEL:
                break;
        }
        return true;
    }
}
