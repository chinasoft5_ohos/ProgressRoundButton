package com.xiaochen.progressroundbutton;

/**
 * Created by tanfujun on 10/26/16.
 * <p>
 * 按钮控制器
 */
public interface ButtonController {
    /**
     * 获取按下色值
     *
     * @param color 颜色色值
     * @return int
     */
    int getPressedColor(int color);

    /**
     * 获取颜色亮度值
     *
     * @param color 颜色色值
     * @return int
     */
    int getLighterColor(int color);

    /**
     * 获取颜色灰值
     *
     * @param color 颜色色值
     * @return int
     */
    int getDarkerColor(int color);

    /**
     * 设置是否默认按压
     *
     * @return boolean
     */
    boolean enablePress();

    /**
     * 设置是否默认渐变色
     *
     * @return boolean
     */
    boolean enableGradient();
}

