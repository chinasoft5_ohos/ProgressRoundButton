ProgressRoundButton
====================

### 项目介绍
+ 项目名称：ProgressRoundButton
+ 所属系列：openharmony的第三方组件适配移植
+ 功能：实现一个平滑的下载按钮
+ 项目移植状态：主功能完成
+ 调用差异：无
+ 开发版本：sdk6，DevEco Studio2.2 Beta1
+ 基线版本：release 2.0.0


### 效果演示
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/163748_4844a600_853727.gif "ProgressRoundButton效果1.gif")
![输入图片说明](https://images.gitee.com/uploads/images/2021/0727/163803_396ae6df_853727.gif "ProgressRoundButton效果2.gif")

### 安装教程

**1)** 在项目根目录下的build.gradle文件中，

```
allprojects {
     repositories {
         maven {
             url 'https://s01.oss.sonatype.org/content/repositories/releases/'
         }
     }
   }
```
**2)** 在entry模块的build.gradle文件中，
```
dependencies {
      implementation('com.gitee.chinasoft_ohos:ProgressRoundButton:1.0.0')
      ......  
   }
```
在sdk6，DevEco Studio2.2 Beta1项目可直接运行 如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件， 并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

### 使用说明
**1)** 您可以这样在xml中定义按钮AnimDownloadProgressButton
```java
        <com.xiaochen.progressroundbutton.AnimDownloadProgressButton
            ohos:id="$+id:anim_btn"
            ohos:height="40vp"
            ohos:width="match_parent"
            ohos:focusable="focus_enable"
            ohos:focusable_in_touch="true"
            hap:progressbtn_background_color="#ffffbb33"
            hap:progressbtn_background_second_color="#aaa"
            hap:progressbtn_text_size="25"/>

```
**2)** 如果您想要按压效果，请使用AnimButtonLayout
```java
        <com.xiaochen.progressroundbutton.AnimButtonLayout
            ohos:id="$+id:anim_btn3"
            ohos:height="40vp"
            ohos:width="match_parent"
            ohos:top_margin="40vp"
            hap:progressbtn_background_color="#ff0099cc"
            hap:progressbtn_background_second_color="#aaa"
            hap:progressbtn_enable_gradient="true"
            hap:progressbtn_enable_press="true"/>
``` 

### 测试信息
CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


### 版本迭代
- 1.0.0
+ 0.0.1-SNAPSHOT

### 版权和许可信息
```
Copyright 2015 cctanfujun

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```