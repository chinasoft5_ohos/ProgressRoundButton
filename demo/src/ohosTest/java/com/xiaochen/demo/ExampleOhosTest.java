package com.xiaochen.demo;

import com.xiaochen.progressroundbutton.utils.ColorUtil;
import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import ohos.agp.utils.Color;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * 单元测试工具类
 */
public class ExampleOhosTest {
    private static final String COLOR1 = "#3F51B5";
    private static final String COLOR2 = "ff3f51b5";

    /**
     * 测试工具类初始化
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.xiaochen.demo", actualBundleName);
    }

    /**
     * 测试工具类
     */
    @Test
    public void testUtilsIsEmpty2() {
        String colorHex = ColorUtil.isHexString(Color.getIntColor(COLOR1), false);
        Assert.assertEquals(COLOR1, colorHex);
        assertEquals(COLOR1, colorHex);
    }

    /**
     * 测试工具类
     */
    @Test
    public void testUtilsIsEmpty3() {
        String colorHex = ColorUtil.getHexString(Color.getIntColor(COLOR1));
        Assert.assertEquals(COLOR2, colorHex);
        assertEquals(COLOR2, colorHex);
    }
}