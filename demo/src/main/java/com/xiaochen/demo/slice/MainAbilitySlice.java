/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xiaochen.demo.slice;

import com.xiaochen.demo.ResourceTable;
import com.xiaochen.demo.Url;
import com.xiaochen.progressroundbutton.AnimButtonLayout;
import com.xiaochen.progressroundbutton.AnimDownloadProgressButton;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Slider;
import ohos.agp.components.Text;
import ohos.agp.text.RichText;
import ohos.agp.text.RichTextBuilder;
import ohos.agp.text.TextForm;

import ohos.agp.utils.Color;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import ohos.utils.net.Uri;

import static com.xiaochen.demo.Constants.*;

public class MainAbilitySlice extends AbilitySlice {
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
    private AnimDownloadProgressButton mAnimDownloadProgressButton;
    private AnimDownloadProgressButton mAnimDownloadProgressButton2;
    private AnimButtonLayout mAnimButtonLayout;
    private Button mReset;
    private Text mDescription, mUrl, mHope;
    private Slider mSeekBar;
    private boolean isClick = false;
    private String path;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        initView();
        initListener();

        mDescription.setText(TEXT1);
        RichTextBuilder richTextBuilder = new RichTextBuilder();
        RichText build = richTextBuilder
                .mergeForm(new TextForm().setTextSize(45).setTextColor(Color.getIntColor("#747474")))
                .addText(TEXT2)
                .revertForm()
                .mergeForm(new TextForm().setTextColor(Color.getIntColor("#018578")).setTextSize(45).setUnderline(true))
                .addText(path)
                .revertForm()
                .build();
        mUrl.setRichText(build);
        mUrl.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                Intent intent = new Intent();
                intent.setAction("android.intent.action.VIEW");
                intent.setUri(Uri.parse(path));
                intent.addFlags(Intent.FLAG_NOT_OHOS_COMPONENT);
                startAbility(intent);
            }
        });
        mHope.setText(TEXT3);
    }

    private void initListener() {
        mAnimDownloadProgressButton.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showTheButton(ResourceTable.Id_anim_btn);
            }
        });
        mAnimDownloadProgressButton.setFocusChangedListener(new Component.FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean hasFocus) {
                if (hasFocus) {
                    mAnimDownloadProgressButton.setProgressBtnBackgroundColor(Color.getIntColor("#0000ff"));
                } else {
                    mAnimDownloadProgressButton.setProgressBtnBackgroundColor(Color.getIntColor("#00ff00"));
                }
            }
        });
        mAnimDownloadProgressButton2.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showTheButton(ResourceTable.Id_anim_btn2);
            }
        });
        mAnimButtonLayout.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                showTheButton(ResourceTable.Id_anim_btn3);
            }
        });
        mReset.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                mAnimDownloadProgressButton.setState(AnimDownloadProgressButton.NORMAL);
                mAnimDownloadProgressButton.setCurrentText("安装");
                mAnimDownloadProgressButton.setProgress(0);
            }
        });
        mSeekBar.setValueChangedListener(new Slider.ValueChangedListener() {
            @Override
            public void onProgressUpdated(Slider slider, int i, boolean b) {
                mAnimDownloadProgressButton.setButtonRadius((i / 100.0f) * mAnimDownloadProgressButton.getHeight() / 2);
                mAnimDownloadProgressButton.invalidate();
            }
            @Override
            public void onTouchStart(Slider slider) {
            }
            @Override
            public void onTouchEnd(Slider slider) {
            }
        });
    }

    private void initView() {
        Url url = new Url();
        url.setHttps(HTTPS);
        url.setUrl(URL);
        path = url.getHttps() + url.getUrl();

        getWindow().setStatusBarColor(Color.getIntColor("#747474"));
        getWindow().setStatusBarVisibility(Component.VISIBLE);

        mReset = (Button) findComponentById(ResourceTable.Id_reset);
        mSeekBar = (Slider) findComponentById(ResourceTable.Id_seekBar);
        mDescription = (Text) findComponentById(ResourceTable.Id_description);
        mUrl = (Text) findComponentById(ResourceTable.Id_url);
        mHope = (Text) findComponentById(ResourceTable.Id_hope);
        mAnimDownloadProgressButton = (AnimDownloadProgressButton) findComponentById(ResourceTable.Id_anim_btn);
        mAnimDownloadProgressButton.setCurrentText("安装");
        mAnimDownloadProgressButton2 = (AnimDownloadProgressButton) findComponentById(ResourceTable.Id_anim_btn2);
        mAnimDownloadProgressButton2.setCurrentText("安装");
        mAnimDownloadProgressButton2.setTextSize(60);
        mAnimButtonLayout = (AnimButtonLayout) findComponentById(ResourceTable.Id_anim_btn3);
        mAnimButtonLayout.setCurrentText("安装");
        mAnimButtonLayout.setTextSize(60);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    private void showTheButton(int id) {
        switch (id) {
            case ResourceTable.Id_anim_btn:
                if (isClick) {
                    mAnimDownloadProgressButton.setState(AnimDownloadProgressButton.DOWNLOADING);
                    mAnimDownloadProgressButton.setProgressText("下载中", mAnimDownloadProgressButton.getProgress() + 8);
                    if (mAnimDownloadProgressButton.getProgress() + 10 > 100) {
                        mAnimDownloadProgressButton.setState(AnimDownloadProgressButton.INSTALLING);
                        mAnimDownloadProgressButton.setCurrentText("安装中");
                        eventHandler.postTask(() -> {
                            mAnimDownloadProgressButton.setState(AnimDownloadProgressButton.NORMAL);
                            mAnimDownloadProgressButton.setCurrentText("打开");
                        }, 2000);
                    }
                } else {
                    isClick = true;
                }
                break;
            case ResourceTable.Id_anim_btn2:
                mAnimDownloadProgressButton2.setState(AnimDownloadProgressButton.DOWNLOADING);
                mAnimDownloadProgressButton2.setProgressText("下载中", mAnimDownloadProgressButton2.getProgress() + 8);
                if (mAnimDownloadProgressButton2.getProgress() + 10 > 100) {
                    mAnimDownloadProgressButton2.setState(AnimDownloadProgressButton.INSTALLING);
                    mAnimDownloadProgressButton2.setCurrentText("安装中");
                    eventHandler.postTask(() -> {
                        mAnimDownloadProgressButton2.setState(AnimDownloadProgressButton.NORMAL);
                        mAnimDownloadProgressButton2.setCurrentText("打开");
                    }, 2000);
                }
                break;
            case ResourceTable.Id_anim_btn3:
                mAnimButtonLayout.setState(AnimDownloadProgressButton.DOWNLOADING);
                mAnimButtonLayout.setProgressText("下载中", mAnimButtonLayout.getProgress() + 8);
                if (mAnimButtonLayout.getProgress() + 10 > 100) {
                    mAnimButtonLayout.setState(AnimDownloadProgressButton.INSTALLING);
                    mAnimButtonLayout.setCurrentText("安装中");
                    eventHandler.postTask(() -> {
                        mAnimButtonLayout.setState(AnimDownloadProgressButton.NORMAL);
                        mAnimButtonLayout.setCurrentText("打开");
                    }, 2000);
                }
                break;
        }


    }
}
