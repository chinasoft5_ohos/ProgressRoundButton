/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.xiaochen.demo;

/**
 * Created by kongzheng on 07/14/21.
 * <p>
 * 常量类
 */
public class Constants {
    /**
     * 文本一
     */
    public static final String TEXT1 = "This is a DownloadProgressButton library with Animation, you can change radius," +
            "textColor,coveredTextColor,backgroundColor,etc in your code or just in xml. \n";

    /**
     * 文本二
     */
    public static final String TEXT2 = "The library is open source in github ";

    /**
     * HTTPS头
     */
    public static final String HTTPS = "https://";

    /**
     * 项目URL路径
     */
    public static final String URL = "github.com/cctanfujun/ProgressRoundButton";

    /**
     * 文本三
     */
    public static final String TEXT3 = "Hope you like it ";
}
